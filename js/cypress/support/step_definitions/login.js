import props from '../../../cypress.json';

var nOfBrowsers = 0;

beforeEach(() => {
  nOfBrowsers += 1;
});

given("the user is on the login page", () => {
  
  if(nOfBrowsers <= 1) {
    cy.visit(props.baseUrl)
  } else {
    cy.reload()
  }

  cy.get('header > h1').should('contain', 'Worlds Best App')
  cy.url().should('eq', props.baseUrl)
});

when("the user enters username", () => {
  cy.get('input[name=email]').type(props.validUsername)
});

when("the user enters a non valid username", () => {
  cy.get('input[name=email]').type(props.nonValidUsername)
});

when("the user enters password", () => {
  cy.get('input[name=password]').type(props.validPassword)
});

when("the user enters a wrong password", () => {
  cy.get('input[name=password]').type(props.wrongPassword)
});

when("clicks Login", () => {
  cy.get('.f5').click()
});

then("the user is presented with a welcome message", () => {
  cy.get('.pa4').should('contain', 'Welcome Dr I Test')
  cy.get('input[name=email]').should('not.contain', props.nonValidUsername)
  cy.get('input[name=password]').should('not.contain', props.validPassword)
});

then("the user is presented with a error message", () => {
  cy.get('#login-error-box').should('contain', 'Credentials are incorrect')
});

then("the user is presented with non valid email error message", () => {
  cy.get('#login-error-box').should('not.contain', 'Credentials are incorrect')
  cy.get('.f5').should('contain', 'Login')
});
