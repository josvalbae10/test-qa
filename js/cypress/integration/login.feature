Feature: Login
  In order to use the app the user must be able to Login

  Scenario: Login Success
    Given the user is on the login page
    When the user enters username
    And the user enters password
    And clicks Login
    Then the user is presented with a welcome message

  Scenario: Login Incorrect password
    Given the user is on the login page
    When the user enters username
    And the user enters a wrong password
    And clicks Login
    Then the user is presented with a error message
  
  Scenario: Login non valid username
    Given the user is on the login page
    When the user enters a non valid username
    And the user enters a wrong password
    And clicks Login
    Then the user is presented with non valid email error message