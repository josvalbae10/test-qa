# Technical Test

## Getting Started

Download the test file and uncompress it [here](https://cdn.glitch.com/cd885eee-f9d0-40ff-b200-f8b9553ca169%2Fqa-test.zip?v=1563353312572)

### Information you will need

- The url for you want to test against is `https://sprinkle-burn.glitch.me/`
- A valid user credentials are below
  - email: `test@drugdev.com`
  - password: `supers3cret`

## Javascript

1.  Fix broken tests
    - Open `test/js/cypress/support/step_definitions/login.js`
    - You will find that the methods have not been completed as they raise the `Not implemented` Error.
    - According to the feature scenario, fill in the necessary steps to pass the test.
2.  What changes could you make to the features to make them better for testing?
    1.  There a few elements in the DOM whose locator would need to be refactored to be able to peform a better testing (id /unique meaningful class name missing)
        1. class=header bg-black-80 white
        2. class=ma0 mb2
        3. aria-label=login
   
    2.  support/steps_definitions -> both typing the username and clicking the login button are actions that should be wrapped around a "when" clause rather than "then" clause as none of them are assertions.
    
    3.  200 status code should not be returned if the login is not successful. Instead, we should return a 401 status code (Unauthorized)
   
3.  Are there any features that you think should be added to improve the testing?
    1. Both "the user has the correct credentials" and "the user has the incorrect credentials" keywords can be replaced by just one that would either navigate to the website or reload the page(clear out the form) depending on the case.
    
    2.  Login button should be disabled as a default state and only get enabled when the user introduced a valid format email address and any password.
        1.  At unit level we should cover:
            1.  the fact that user needs to introduce a valid email address (contains @ and a domain)
    
    3.  The email input field seems not to have any limit in terms of the number of characters that the user can introduce and the warning toast message can potentially occupy all the screen unnecessarily.
    
    4.  I wrote a "test case scenario" for testing the warning toast message (Login non valid username) but I would cover it at unit level. 

## Postman

### Fix broken postman tests

- Install and Open PostMan on your machine.
- Open the collection `test/postman/collections/WorldsBestApp.json`
- Setup the environment `test/postman/environments/local.json`

1.  Try to run it and you'll find that some of the tests are broken. Fix them.
2.  Can the test be improved in anyway?
    1.  Login error scenario -> 200 status code is returned but it should be 401 (Unauthorize)
    2.  Username and password(s) can be handled at configuration level i.e in the local.json file.

---

# Tips

## Requirements

- [nodejs >= 8](https://nodejs.org/en/)
- [Postman](https://www.getpostman.com/)

## Install on Mac

```
brew install node
brew cask install postman
brew cask install chromedriver
```

# Install on Windows

```
choco install selenium-chrome-driver
choco install nodejs
choco install postman
```

## Javascript and Cypress

[cypress](https://cypress.io) is the test framework. It also uses the [cucumber plugin](https://github.com/TheBrainFamily/cypress-cucumber-preprocessor) it is already configured for you.

### Setup

Make sure you have everything installed above

```
cd js
npm i
```

### Running tests

`npm test`

## Postman

Check that the apis are returning the desired results. You can use the ui tool information for importing the files can be found [here](https://www.getpostman.com/docs/v6/postman/collections/data_formats). If you wish to use the cli
tool directions are below.

### Running Postman Test cli

### Run all

`npx newman run postman/collections/*.json -e postman/environments/local.json`

### Run one

`npx newman run postman/collections/WorldsBestApp.json -e postman/environments/local.json`
